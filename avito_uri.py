from enum import Enum, IntFlag
from urllib.parse import quote

AVITO_SEARCH_URI_TEMPLATE = "https://www.avito.ru/{0}/avtomobili"
year_dict = {
    -1: "0",
    2000: "893",
    2001: "894",
    2002: "895",
    2003: "896",
    2004: "897",
    2005: "898",
    2006: "899",
    2007: "900",
    2008: "901",
    2009: "902",
    2010: "6045",
    2011: "8581",
    2012: "6045",
    2013: "8581",
    2014: "11017",
    2015: "13978",
    2016: "16381",
    2017: "19775",
    2018: "20303",
    2019: "405242"
}

mileage_dict = {
    -1: "0",
    10: "15487",
    20: "15492",
    30: "15496",
    40: "15500",
    50: "15505",
    60: "15509",
    70: "15512",
    80: "15516",
    90: "15520",
    100: "15524",
    110: "15527",
    120: "15528",
    130: "15531",
    140: "15533",
    150: "15535",
    200: "15544",
}


class OwnerCount(Enum):
    ONE = 1,
    TWO = 2,
    THREE_AND_MORE = 3


class PowerTrain(IntFlag):
    FWD = 1
    RWD = 2
    AWD = 4


class Gearbox(IntFlag):
    Mechanical = 1
    Auto = 2
    CVT = 4
    Robot = 8


class AvitoUriBuilder:
    location = "yoshkar-ola"
    location_radius = -1
    query = None

    car_brand = None
    car_model = None

    price_min = -1
    price_max = -1

    year_min = -1
    year_max = -1

    mileage_min = -1
    mileage_max = -1

    engine_size_min = -1
    engine_size_max = -1

    engine_power_min = -1
    engine_power_max = -1

    was_in_accident = False

    powertrain = None
    gearbox = None
    owner_count = None

    __uri = ""
    __is_first_parameter = True
    __additional_data = dict()

    def __init__(self):
        self.__uri = ""
        self.__is_first_parameter = True;

    def generate_uri(self):
        self.__uri = AVITO_SEARCH_URI_TEMPLATE.format(self.location)

        if self.car_brand != None:
            self.__uri += "/{0}".format(self.car_brand)
        if self.car_brand != None and self.car_model != None:
            self.__uri += "/{0}".format(self.car_model)

        if self.owner_count == OwnerCount.ONE:
            self.__uri += "/odin_vladelec"
        if self.owner_count == OwnerCount.TWO:
            self.__uri += "/ne_bolee_dvuh"
        if self.owner_count == OwnerCount.TWO:
            self.__uri += "/tri_i_bolee"

        if self.gearbox != None:
            gearbox_data = []
            if Gearbox.Mechanical in self.gearbox:
                gearbox_data.append("860")
            if Gearbox.Auto in self.gearbox:
                gearbox_data.append("861")
            if Gearbox.CVT in self.gearbox:
                gearbox_data.append("14754")
            if Gearbox.Robot in self.gearbox:
                gearbox_data.append("14753")
            if len(gearbox_data) != 0:
                self.__additional_data["185"] = str.join(',', gearbox_data)

        if self.mileage_min > 0 or self.mileage_max > 0:
            mileage_data = "{0}b{1}".format(mileage_dict[self.mileage_min], mileage_dict[self.mileage_max])
            self.__additional_data['1375'] = mileage_data
        if self.year_max > 0 or self.year_min > 0:
            year_data = "{0}b{1}".format(year_dict[self.year_min], year_dict[self.year_max])
            self.__additional_data['188'] = year_data

        if self.powertrain != None:
            powertrain_data = []
            if PowerTrain.FWD in self.powertrain:
                powertrain_data.append("8851")
            if PowerTrain.RWD in self.powertrain:
                powertrain_data.append("8852")
            if PowerTrain.AWD in self.powertrain:
                powertrain_data.append("8853")
            if len(powertrain_data) != 0:
                self.__additional_data["695"] = str.join(',', powertrain_data)

        self.add_parameter("p", "{0}")

        if (self.query != None):
            self.add_parameter("q", quote(self.query))

        if self.location_radius > 0:
            self.add_parameter("radius", self.location_radius)

        if self.price_min > 0:
            self.add_parameter("pmin", self.price_min)
        if self.price_max > 0:
            self.add_parameter("pmax", self.price_max)

        if len(self.__additional_data) > 0:
            additional_data = ""
            keys = list(self.__additional_data.keys())
            keys.sort();
            for key in keys:
                additional_data += "{0}_{1}.".format(key, self.__additional_data[key])
            self.add_parameter("f", additional_data)

        return self.__uri

    def add_parameter(self, parameter, value):
        if self.__is_first_parameter:
            self.__is_first_parameter = False
            self.__uri += "?{0}={1}".format(parameter, value)
        else:
            self.__uri += "&{0}={1}".format(parameter, value)
