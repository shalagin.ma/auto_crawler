from datetime import datetime
import re

import requests
from lxml import html


from avito_uri import *

AVITO_SEARCH_URI_TEMPLATE = "https://www.avito.ru/yoshkar-ola/avtomobili?radius=300"
__result = []


def generate_report(obj_list):
    filename = "{0}.csv".format(datetime.timestamp(datetime.now()))
    print("Writing to " + filename)
    f = open(filename, "w+")
    header = ""
    header += "name\t"
    header += "price\t"
    header += "year\t"
    header += "mileage\t"
    header += "dtp\t"
    header += "transmission\t"
    header += "engine_power\t"
    header += "powertrain\t"
    header += "body\t"
    header += "fuel\t"
    header += "city\t"
    header += "link\t"
    f.write(header)
    f.write("\n")

    for obj in obj_list:
        line = ""
        line += str(obj["name"])
        line += '\t'
        line += str(obj["price"])
        line += '\t'
        line += str(obj["year"])
        line += '\t'
        line += str(obj["mileage"])
        line += '\t'
        line += str(obj["dtp"])
        line += '\t'
        line += str(obj["transmission"])
        line += '\t'
        line += str(obj["engine_power"])
        line += '\t'
        line += str(obj["powertrain"])
        line += '\t'
        line += str(obj["body"])
        line += '\t'
        line += str(obj["fuel"])
        line += '\t'
        line += str(obj["city"])
        line += '\t'
        line += ("https://www.avito.ru/" + obj["link"])
        f.write(line)
        f.write("\n")
    f.flush()
    f.close()


def crawl(uri_generator):
    __result.clear()
    print("Going to Avito server...")
    has_something_to_parse = True
    page = 1
    avito_uri = uri_generator.generate_uri()
    while has_something_to_parse:
        has_something_to_parse = crawl_to_avito(avito_uri.format(page))
        page += 1

    if len(__result) > 0:
        generate_report(__result)
    else:
        print("Captcha fucked me over")


def crawl_to_avito(uri):
    print('Request by url {0}'.format(uri))

    r = requests.get(uri)
    if r.status_code == 200:
        filename = "{0}.html".format(datetime.timestamp(datetime.now()))
        print("OK!")
        return parse(r.text)
    else:
        print('Something went wrong, error code : {0}'.format(r.status_code))
        return False


def parse(text):
    data = html.fromstring(text)
    content = data.cssselect("div.item__line")
    for list_item in content:
        parsed_obj = parse_list_item(list_item)
        __result.append(parsed_obj)
    return len(content) > 0


def parse_list_item(list_dom):
    price_elements = list_dom.cssselect("span.price")
    header_elements = list_dom.cssselect("h3")
    link_elements = list_dom.cssselect("a.item-description-title-link")
    specific_params = list_dom.cssselect("div.specific-params")
    city_params = list_dom.cssselect(".item-address")
    res = dict()
    if len(price_elements) > 0:
        price_text = price_elements[0].text_content().strip()
        price_text = re.sub("[^0-9]", "", price_text)
        res["price"] = int(price_text)
    if len(header_elements) > 0:
        header_text = header_elements[0].text_content().strip()
        split = header_text.split(',')
        res['name'] = split[0]
        res['year'] = int(split[1])
    if len(specific_params) > 0:
        specific_text = specific_params[0].text_content().strip()
        split = specific_text.split(',')
        if split[0] == "Битый":
            res['dtp'] = True
            split.pop(0)
        else:
            res['dtp'] = False
        mileage_text = re.sub("[^0-9]", "", split[0])
        res['mileage'] = int(mileage_text)
        res['body'] = split[2].strip()
        res['powertrain'] = split[3].strip()
        res['fuel'] = split[3].strip()

        engine_data = split[1]
        res["engine_size"] = float(re.findall("[0-9]\.[0-9]", engine_data)[0])
        res['transmission'] = re.findall(".T", engine_data)[0]
        power_data = re.findall("\(\d+ л.с.\)", engine_data)
        res['engine_power'] = int(re.sub("[^0-9]", "", power_data[0]))
    if len(city_params) > 0:
        res['city'] = city_params[0].text_content().strip()
    if len(link_elements) > 0:
        res['link'] = link_elements[0].xpath("@href")[0]

    return res


# parse('<html><head>Header</head><body><div class=item>Div content</div>Main body</body></html>')
# parse_test_file()
# main()
uri_generator = AvitoUriBuilder()
uri_generator.owner_count = OwnerCount.ONE
uri_generator.mileage_min=10
uri_generator.car_brand = "skoda"
uri_generator.car_model = "rapid"
uri_generator.year_min = 2014
uri_generator.year_max = 2018
uri_generator.location = "rossiya"
crawl(uri_generator)
